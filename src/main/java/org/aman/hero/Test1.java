/**
 * 
 */
package org.aman.hero;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @Copyright: 本内容仅限于重庆爱赢科技有限公司内部使用，禁止转发. 
 * @Author: daijiangguo 2014年7月17日 上午11:58:10 
 * @Version: $Id$
 * @Desc: <p></p>
 */
public class Test1 {
/*
 给定整数区间[A,B]问其中有多少个完全平方数。 输入格式： 多组数据，包含两个正整数A,B 1<=A<=B<=2000000000。 输出格式： 每组数据输出一行包含一个整数，表示闭区间[A,B]中包含的完全平方数的个数。 
挑战规则： 
 输入样例 1 1 1 2 3 10 3 3 输出样例： 1 1 2 0
 */
    public static int test1(int s, int e){
        //System.out.println(Math.sqrt(e));
        int min = (int)Math.sqrt(s);
        int max = (int)Math.sqrt(e);
        //System.out.println(min + "=="+max);
        if(min*min!=s){
            min++;
        }
        //System.out.println(min + "=="+max);
        return max - min + 1;
    }
    
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true){
            String line = sc.nextLine();
            String[] arr = line.split(" ");
            if(arr.length == 2){
                System.out.println(test1(Integer.parseInt(arr[0]),Integer.parseInt(arr[1])));
            }
        }
    }
    
}
