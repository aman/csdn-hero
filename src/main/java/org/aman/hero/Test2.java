/**
 * 
 */
package org.aman.hero;

import java.util.Scanner;

/**
 * @Copyright: 本内容仅限于重庆爱赢科技有限公司内部使用，禁止转发. 
 * @Author: daijiangguo 2014年7月17日 下午1:48:24 
 * @Version: $Id$
 * @Desc: <p></p>
 */
public class Test2 {
/* 
 大家对指数运算都非常熟悉，定义f(n,k) = n ^ k （n的k次方）， 例如f(2, 3) = 8, f(3,4) = 81。 我们定义f(0,0) = 1。 
 我们的目标是给定整数n1,k1,n2,k2,n，求f(f(n1,k1),f(n2,k2)) % n。 （％是取余数运算）。
  输入格式 多组数据，每组数据就一行包含5个整数n1,k1,n2,k2,n。 (0<=n1,k1,n2,k2<=1000000000, 1 <= n <=10000000) 输出格式 每组数据一行，表示最终结果。 
挑战规则： 
 输入样例   1 2 3 4 5 5 4 3 2 1 输出样例   1 0
 */
    public static long test(long n1, long k1, long n2, long k2, long n) {
        long result = -1;
        long nn = test(n1, k1, n);
        for (; k2 > 0; k2--) {
            if (k2 == 1) {
                return test(nn, n2, n);
            } else if (k2 > 1) {
                nn = test(nn, n2, n);
            }
        }
        return result;
    }

    public static long test(long n1, long k1, long n) {
        if (k1 == 1) return n1 % n;
        long s = test(n1, k1 / 2, n);
        if (k1 % 2 == 0) {
            return s * s % n;
        } else {
            return (s * s % n) * n1 % n;
        }
    }
    
    public static void main(String[] args) {
        long dd = System.currentTimeMillis();
        System.out.println(test(324143332, 324134132, 213432143, 135223101,349768658));
        System.out.println(System.currentTimeMillis() - dd);
    }
}
